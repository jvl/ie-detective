IE Detective

What is it?
A script written in JavaScript to detect different Internet Explorer versions. A class depending upon the version of IE is added to the <html> tag. (If the version supports it.)

How do I use it?
Simply include the file like any other script:

<script type="text/javascript" src="js/ie-detective.js"></script>

What IE versions are supported?
IE 3+ - every version that supports JavaScript!

What classes are added to the <html> tag?
IE10
IE9
IE8
IE7
IE6
IE55
IE501

Note: IE4 does not support classes on the <html> tag.
      You can, however redirect users to another page.
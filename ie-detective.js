if(document.all){
    if(window.atob){
        //IE10
        var elements = document.getElementsByTagName('html');
        elements[0].className += " IE10";
        elements[0].className += " IE";
    }
    else if(document.getElementsByClassName){
        //IE9
        var elements = document.getElementsByTagName('html');
        elements[0].className += " IE9";
        elements[0].className += " IE";
    }
    else if(document.querySelector){
        //IE8
        var elements = document.getElementsByTagName('html');
        elements[0].className += " IE8";
        elements[0].className += " IE";
    }
    else if(window.XMLHttpRequest){
        //IE7
        var elements = document.getElementsByTagName('html');
        elements[0].className += " IE7";
        elements[0].className += " IE";
    }
    else if(document.compatMode){
        //IE6
        var elements = document.getElementsByTagName('html');
        elements[0].className += " IE6";
        elements[0].className += " IE";
    }
    else if(window.createPopup){
        //IE5.5
        var elements = document.getElementsByTagName('html');
        elements[0].className += " IE55";
        elements[0].className += " IE";
    }
    else if(window.attachEvent){
        //IE5.0.1
        var elements = document.getElementsByTagName('html');
        elements[0].className += " IE501";
        elements[0].className += " IE";
    }
    //IE4 doesn't support classes on the html tag, they do however support redirects.
    else if(document.images){
        //IE4
        window.location.replace("http://www.december.com/html/demo/hello.html");
    }
}
//If you wish to target users who use a sensible browser, use the following block.
else{
    var elements = document.getElementsByTagName('html');
    elements[0].className += " notIE";
}